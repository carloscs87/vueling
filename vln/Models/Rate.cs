﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vln.Models
{
    public class Rate
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal rate { get; set; }
    }
}