﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vln.Models
{
    public class Transaction
    {
        public string Sku { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}