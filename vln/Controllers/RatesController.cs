﻿using vln.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json;

namespace vln.Controllers
{
    public class RatesController : ApiController
    {
        public IEnumerable<Rate> GetAllRates()
        {
            return GetRates();
        }

        private Rate[] GetRates()
        {
            var result = new Rate[] { };
            using(WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://quiet-stone-2094.herokuapp.com/rates.json");
                result = JsonConvert.DeserializeObject<Rate[]>(json);
            }
            return result;
        }
    }
}
