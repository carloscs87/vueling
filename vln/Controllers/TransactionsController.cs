﻿using vln.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json;

namespace vln.Controllers
{
    public class TransactionsController : ApiController
    {

        public IEnumerable<Transaction> GetAllTransactions()
        {
            return GetTransactions();
        }

        public IHttpActionResult GetTransaction(string sku)
        {
            var transactions = GetTransactions();
            var trans = transactions.Where((p) => p.Sku == sku).ToArray();
            if (trans == null || trans.Count() == 0)
            {
                return NotFound();
            }
            return Ok(CurrencyToEur(trans));
        }

        private Transaction[] GetTransactions()
        {


            var result = new Transaction[] { };

            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://quiet-stone-2094.herokuapp.com/transactions.json");
                result = JsonConvert.DeserializeObject<Transaction[]>(json);
            }

           return CurrencyToEur(result).ToArray();
            //return result;
        }

        private List<Transaction> CurrencyToEur(Transaction[] transactions)
        {
            var rates = GetRates();
            var result = new List<Transaction>();
            foreach (var transaction in transactions)
            {
                while (transaction.Currency != "EUR")
                {

                    var rateEUR = rates.FirstOrDefault(r => r.To == "EUR");
                    var rate = rates.FirstOrDefault(p => p.From == transaction.Currency);

                    if (transaction.Currency == rateEUR.From)
                    {
                        rate = rateEUR;
                    }
                    transaction.Amount = decimal.Multiply(transaction.Amount, rateEUR.rate);
                    transaction.Currency = rate.To;
                }
                result.Add(transaction);
            }
            return result;
        }

        private Rate[] GetRates()
        {
            var result = new Rate[] { };
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString("http://quiet-stone-2094.herokuapp.com/rates.json");
                result = JsonConvert.DeserializeObject<Rate[]>(json);
            }
            return result;
        }
    }
}
